---
title: Setting up https on a schizophrenic machine
subtitle: How people living in the past are a pain in the \*\*\*
author: Moviuro
keywords:
- tls
- haproxy
- nginx
- networking
date: 2017-04-11
lang: en-US
---

Okay, so I recently set out to deploy HTTPS to this website, mainly for fun and
giggles.

# Let's Encrypt

If you've been living in a cave for the last years,
[letsencrypt](https://letsencrypt.org/) is a *free, automated and open
Certificate Authority*. It comes with a sweet, sweet tool to automatically
generate your server's TLS keys, and renew your certificates if they get close
to expiration. This has been detailled in [this FreeBSD wiki
article](https://wiki.freebsd.org/BenWoods/LetsEncrypt) that I won't be
paraphrasing here.

# Schizophrenic

My dedicated server is pretty much schizophrenic right now. With only one single
IPv4 address, and a virtually endless supply of IPv6 address, it's ridiculously
easy to deploy new services to the world living in the future. However, tons of
people still live in the dark ages of the Interwebz and don't have
[IPv6](https://en.wikipedia.org/wiki/IPv6) ([RFC
2460](https://tools.ietf.org/html/rfc2460) was published in December **1998**).

So on we go, to the wonderful country of dual-stack services, in which IPv4
requires more work!

![](images/problem.svg)

**Goals**:

* Have IPv6 and IPv4 clients reach my blog over *https*, without blocking
the future possibilities of adding new domains and webservers to my machine.
* Keep TLS secrets in the single place where they are needed.

A simple `tcp/443` redirect was out of the question, as well as TLS offloading.

## nginx

I first thought of nginx, for it's the reverse proxy I use anyway and it has a
[module](https://nginx.org/en/docs/stream/ngx_stream_ssl_preread_module.html)
that can extract the [SNI](https://en.wikipedia.org/wiki/Server_Name_Indication)
information from a TLS connection, and use this information to smartly proxy
https connections.

This module however requires unusual build options (namely
`--with-stream_ssl_preread_module`), and there were no prebuilt package with
this option for my FreeBSD server. Hm.

## haproxy

Here it is: [the
solution](https://www.haproxy.com/documentation/aloha/10-0/deployment-guides/tls-infrastructure/#ssl-tls-pass-through).
It does exactly TLS passthrough, with smart SNI "redirection" or whatever this
thing is called.

![TLS passthrough diagram](images/haproxy-tls.svg)

# setup

Now, this might look overkill, but this is the current setup on my machine:

![Dual-stack setup](images/setup-complete.svg)

And this would be the setup with no support with IPv4:

![IPv6-only setup](images/setup-simple.svg)

Maybe I'll see this setup during my lifetime.

I mean, come on, IPv4 support requires that I use yet another software... and
everybody knows that more software = more issues.
