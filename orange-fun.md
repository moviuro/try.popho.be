---
title: Orange fun
subtitle: How changing ISP is !fun!
author: Moviuro
keywords:
- dns
- projects
date: 2018-02-15
lang: en-US
---

# Switch

Recently switched over from [Free](https://free.fr) to
[Orange](https://orange.fr).

## Pros

* Fiber: 200Mbps Down, 50Mbps Up, up from 16Mbps Down and... nvm
* Cheaper. Yup.
* Customer-facing service actually exists. (Responds in < 2 months)

## Cons

* No fixed IPv4 address
* No IPv6 configuration; only the on-off switch. Nothing like "Next Hop", etc.
* No [TFx channels](https://www.journaldugeek.com/2018/02/13/fin-de-diffusion-imminente-chaines-tf1-orange/),
  TF1 wants money, Orange won't give in... let's see where it goes

## Consequences

* Downloading games has never been this fast
* DynDNS is stupid, simple, I like it
* Must use DynDNS
* Must deploy my own [IPv6 tunnel](https://tunnelbroker.net)
* IPv6-readiness and availability down (because it relies on a valid IPv4
  address)
* Unhappy girlfriend

# DynDNS

Problem: changing IPv4 address. My NAS would change IP regularly, because *"The
fixed IP option is not compatible with fiber"* [Orange
Conseil](https://twitter.com/@Orange_conseil).

Solution: DynDNS. Make a regular `curl(1)` call to the specific endpoint, and
TADA.

```{filename="crontab(5)"}
#!/bin/sh

OVH_USER="domain.ovh-user"
OVH_KEY='123456abcdef'
OVH_HOST="local.domain.ovh"

PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin

*/5  *    *    *    *    curl -4s --user "$OVH_USER:$OVH_KEY" "https://www.ovh.com/nic/update?system=dyndns&hostname=${OVH_HOST}"
```

Thanks [Wojciech Langiewicz](https://www.wlangiewicz.com/2014/07/23/dyndns-at-ovh/)

# IPv6

## It works...

As usual, [HurricaneElectric](https://tunnelbroker.net) provides! *Click click
click*, new tunnel up and running.

Same as above for DynDNS updates:

```{filename="crontab(5)"}
*/5  *    *    *    *    curl -4s --user "$TB_USER:$TB_KEY" "https://ipv4.tunnelbroker.net/nic/update?hostname=${TB_ID}"
```

API is documented [here](https://forums.he.net/index.php?topic=3153.0).

## ...not!

Stupid issues, first identified because of very, very low speed when downloading
from a FreeBSD repo. Some `tcpdump(1)`, `ping(1)` later, here's an issue that
even Google doesn't know about:

    00:00:04.045368 AF IPv6 (28), length 84: version error: 0 != 6

Which, of course, is as helpful as it's expressive. Maybe a broken config on my
end? After all, I'm still at the beginner level of networking. Wrong
`prefixlen`? Nope, changing it breaks even `ping6`. Oops.
