#!/bin/sh

## Copyright 2017-2020 Moviuro <moviuro at gmail dot com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

set -e

# Clean previous files
[ -d site ] && rm -r site
[ -d include ] && rm -r include

# Create dirs
mkdir -p include

# Generate the navigation bar
printf '<div class="%s">' 'navigation' > include/navigation
printf '<nav id="%s">\n' 'bydate' >> include/navigation
printf '<h1 id="%s">%s</h1>\n' 'articles' 'articles' >> include/navigation

## Find all blog articles
find . -maxdepth 1 -type f -name '*.md' |
## Extract title and date
{ while IFS= read -r _file; do
  _otitle="$(basename "$_file")"
  _otitle="${_otitle%%.md}"
  title="$(pandoc -t json $_file |
    jq '[.meta.title.c[].c] | join(" ")' |
    tr -d \")"
  date="$(pandoc -t json $_file | jq '.meta.date.c[].c' | tr -d \")"
  ## Generate a short markdown list of all articles
  printf '* [%s %s](/%s.html)\n' "$date" "$title" "$_otitle" >> include/article_list
  ## We also work on the keywords
  pandoc -t json "$_file" |
    jq '.meta.keywords.c[].c[].c | if . != null then . else empty end | @text' | \
     tr -d '"' |
      while IFS= read -r _tag; do
        # create a markdown list for a specific tag
        printf '* [%s](/%s.html)\n' "$title" "$_otitle" >> "tag_$_tag"
        # list "$date URL" for all articles. We catch them, and sort them to get
        # the latest post that will be symlinked as index.html
        printf '%s /%s.html\n' "$date" "$_otitle"
        # add the tags of the current post to the list of all tags (and don't
        # care for duplicates), we will use this list to generate the links to
        # the respective tags pages.
        printf '%s /tag_%s.html\n' "$_tag" "$_tag" >> include/tags.raw
      done
done }

sort include/tags.raw | uniq -c |
while IFS=' 	' read -r _tag_count _tag _tag_link; do
  printf '[%s^%s^](%s)\n' "$_tag" "$_tag_count" "$_tag_link" >> include/tags
done

sort -n include/article_list |
## This list becomes our navigation list by date
pandoc -t html >> include/navigation
rm include/article_list
printf '</nav>\n' >> include/navigation

# We now work on tags
printf '<nav id="%s">\n' 'bytag' >> include/navigation
printf '<h1 id="%s">%s</h1>\n' 'tags' 'tags' >> include/navigation

sort -u include/tags | pandoc -t html >> include/navigation
printf '</nav>\n' >> include/navigation

find . -name '*.nav' | sort -n |
{ while IFS= read -r _file; do
  _id="${_file%.nav}" ; _id="${_id#*-}"
  printf '<div id="%s">' "div_$_id"
  pandoc -f markdown -t html "$_file"
  printf '</div>'
done } >> include/navigation

printf '</div>\n' >> include/navigation

## The navigation panel is finished, we can now use it to generate the pages

# For all tag_* pages
find . -maxdepth 1 -name 'tag_*' -type f |
{ while IFS= read -r _file; do
    # We pipe some markdown to pandoc
    { cat << EOF
---
title: Tagged in *${_file##*/tag_}*
lang: en-US
---
EOF
     sort -u "${_file}"; } |
     pandoc -c /simple.css -A include/navigation \
      --template=template/try.popho.be -s -t html -o $_file.html
  done }

# Generate the webpages
## Find all blog articles
find . -maxdepth 1 -type f -name '*.md' |
{ while IFS= read -r _file; do
  ## Generate the HTML files
  _otitle="$(basename "$_file")"
  _otitle="${_otitle%%.md}"
  pandoc -s --template=template/try.popho.be -A include/navigation -c \
   /simple.css -o "$_otitle.html" "$_file"
done }

# Generate the error pages
find . -maxdepth 1 -type f -name '*.err' |
{ while IFS= read -r _file; do
  ## Generate the HTML files
  _otitle="$(basename "$_file")"
  _otitle="${_otitle%%.err}"
  pandoc -s --template=template/try.popho.be -A include/navigation -c \
   /simple.css -f markdown -o "$_otitle.html" "$_file"
done }

# generate pictures
if [ -d images ]; then
  cd images
  find . -name '*.dot' | {
    while IFS= read -r _file; do
      _otitle="$(basename "$_file")"
      _otitle="${_otitle%%.dot}"
      dot -Tsvg "$_file" -o "$_otitle".svg
    done
  }
  cd ..
fi

mkdir -p site/images
find . -maxdepth 1 -name '*.html' -exec mv {} site \;

# Add anchor links to a § symbol
for _file in site/*html; do
  sed -E -e 's!<h(.?) id="([^>"]+)">!<h\1 id="\2"><a href="\#\2" class="anchor">§</a> !' \
    "$_file" > "$_file".2
  mv "$_file".2 "$_file"
done

# "Minify" CSS (remove comments, newlines and spaces)
# https://stackoverflow.com/questions/1251999/how-can-i-replace-a-newline-n-using-sed

# Replace the end of comments with an otherwise unseen symbol, then match
# and remove.
# -e 's/\*\//❤/g' -e 's/\/\*[^❤]*❤//g'

# The parenthesis () cannot be added here (causes issues with media
# queries)
# -e 's/ \+\([{};]\)/\1/g'
find . -maxdepth 1 -name '*.css' |
  while IFS= read -r _css_file; do
    sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/ /g' \
     -e 's/\*\//❤/g' -e 's/\/\*[^❤]*❤//g' \
     -e 's/ \+\([{};]\)/\1/g' \
     -e 's/\([^a-z0-9-]\) \+/\1/g' < \
    "$_css_file" > site/"$_css_file"
  done
find . -maxdepth 2 -name '*.svg'  -exec cp {} site/images \;
find . -maxdepth 1 -name 'tag_*'  -delete
[ -d include ] && rm -r include
