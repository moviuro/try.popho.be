---
title: Bye Unison
author: Moviuro
keywords:
- sync
date: 2015-09-23
lang: en-US
---

As of today, I don’t use [Unison](https://www.cis.upenn.edu/~bcpierce/unison/)
anymore. **No sir**. It was all fun and simple and *Oh look, it syncs!* until
*Oh look, it doesn’t sync anymore* and everything broke down in a fire of
`Uncaught exception Failure("input_value: bad bigarray kind")`.

Because of my gift, I found out the hard way that `unison(1)` actually requires:

* the same version on all the pairs of machines
* compiled against the same version of OCaml

In short, `unison(1)` is unusable in an heterogeneous network of machines. Just
for example, Archlinux had the latest Unison for a while, along with the latest
OCaml. FreeBSD had the same Unison but a different OCaml. What happened then?
**Spoilers: it broke!**

So I gave [syncthing](https://syncthing.net) a shot. What happened? Well:

* p2p
* backwards-compatible (to an extent)
* OS-agnostic

Also, dead-simple to setup. My grandma could probably do it.

The last magic trick of syncthing is that it can run and update on its own. So I
still have an instance running at my school that my sysadmin probably didn’t
notice. (Which is kind of nice, because my backups aren’t quite ready yet, so my
school still handles them: but that’s a story for an other time.)
