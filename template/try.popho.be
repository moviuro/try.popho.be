<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"$if(lang)$ lang="$lang$" xml:lang="$lang$"$endif$$if(dir)$ dir="$dir$"$endif$>
<head>
  <!-- Responsive -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta name="generator" content="pandoc" />
$for(author-meta)$
  <meta name="author" content="$author-meta$" />
$endfor$
$if(date-meta)$
  <meta name="date" content="$date-meta$" />
$endif$
$if(updated-meta)$
  <meta name="updated" content="$updated-meta$" />
$endif$
$if(keywords)$
  <meta name="keywords" content="$for(keywords)$$keywords$$sep$, $endfor$" />
$endif$
  <title>$if(title-prefix)$$title-prefix$ – $endif$$pagetitle$</title>
  <style type="text/css">code{white-space: pre;}</style>
$if(quotes)$
  <style type="text/css">q { quotes: "“" "”" "‘" "’"; }</style>
$endif$
$if(highlighting-css)$
  <style type="text/css">
$highlighting-css$
  </style>
$endif$
$for(css)$
  <link rel="stylesheet" href="$css$" type="text/css" />
$endfor$
$if(math)$
  $math$
$endif$
$for(header-includes)$
  $header-includes$
$endfor$
</head>
<body>
$for(include-before)$
$include-before$
$endfor$
<div id="content">
$if(title)$
<div id="$idprefix$header">
<h1 class="title">$title$</h1>
$if(subtitle)$
<h2>Or <span class="subtitle">$subtitle$</span></h2>
$endif$
$for(author)$
by <span class="author">$author$</span> 
$endfor$
$if(date)$
on <span class="date">$date$</span>
$if(updated)$
(last updated on <span class="updated">$updated$</span>)
$endif$
$endif$
</div>
$endif$

$if(keywords)$
Tagged in: $for(keywords)$<a href="/tag_$keywords$.html">$keywords$</a>$sep$, $endfor$
$endif$

$if(toc)$
<div id="$idprefix$TOC">
$toc$
</div>
$endif$
$body$
</div>
$for(include-after)$
$include-after$
$endfor$
</body>
</html>
