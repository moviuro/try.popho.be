---
title: About trypophobia
author: Moviuro
keywords:
- about
date: 2016-02-22
lang: en-US
---

[Trypophobia](https://en.wikipedia.org/wiki/Trypophobia)  a claimed pathological
fear of holes, particularly irregular patterns of holes. It also happens to be
in the list of fears ending in *popho.be*, and as such had a place as a
subdomain of mine.

As it is the fear of holes, I’m using it to host my blog, and avoid any kind of
information loss, which would be horrific. (See the pattern here?)
