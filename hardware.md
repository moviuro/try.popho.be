---
title: (Shitty) Hardware
subtitle: How good € isn't (always) for good stuff
author: Moviuro
keywords:
- hardware
date: 2018-05-02
lang: en-US
---

1935.40€ is the exact amount I paid for my machine + 27" screen.

Here is the list of hardware: (also on
[PCPartPicker](http://pcpartpicker.com/list/qfDcTH))

* [i5 6600K](https://ark.intel.com/products/88191/Intel-Core-i5-6600K-Processor-6M-Cache-up-to-3_90-GHz)
* [MSI Z170A SLI PLUS](https://www.msi.com/Motherboard/Z170A-SLI-PLUS/Overview)
* [Corsair DDR4 3000MHz 16GB](https://www.corsair.com/us/en/Categories/Products/Memory/VENGEANCE%C2%AE-LPX-16GB-%282x8GB%29-DDR4-DRAM-3000MHz-C15-Memory-Kit---Black/p/CMK16GX4M2B3000C15)
* [Noctua NH-D14](https://noctua.at/en/nh-d14)
* [Sapphire Nitro R9 Fury](https://www.sapphirenation.net/sapphire-nitro-r9-fury-technical-overview)
* [Phanteks P400s](http://phanteks.com/Eclipse-P400S.html) (closed, black)
* 2x[WD20EZRZ](https://www.wdc.com/en-gb/products/internal-storage/wd-blue-desktop.html#WD20EZRZ)
* [Crucial BX200](http://www.crucial.com/usa/en/storage-ssd-bx200)
* [Corsair RM650x](https://archive.md/S4E3M)
* [Asus MG279Q](https://www.asus.com/Monitors/MG279Q/)

So actually, quite a bit of a beast.

# Asus Screen

The M279Q is actually quite nice, comes with two cables (of which a mini-DP to
DP), if memory serves me right. However it has this stupid, stupid flaw:
sometimes, just sometimes, it will display two 1440x1280 zones that overlap on
~20 pixels. The issue is
[known](https://rog.asus.com/forum/archive/index.php/t-87817.html) though. How
it passed QA is a mystery.

There's still a redeeming quality though: if you turn it off and on again, [the
problem might disappear](https://www.youtube.com/watch?v=pjvQFtlNQ-M).

Asus, your screen *sucks*.

# XMP and iGPU

The MoBo has built-in OC capabilities, and also
[XMP](https://en.wikipedia.org/wiki/Serial_presence_detect#XMP). So on my merry
way, I go and activate said XMP to truly enjoy the performance of 3000MHz
memory! *HAHA, well guess what?* [**Can't use your iGPU with
XMP.**](http://www.tomshardware.co.uk/answers/id-2973637/screen-flicker-enabling-xmp.html)
That was in 2016. We're in May **2018** and it still isn't fixed. See also
[this](https://forum-en.msi.com/index.php?topic=268469.0).

The redeeming quality this time, is that I can tweak many voltages in [*Click
BIOS 5*](https://www.msi.com/blog/uefi-bios), including `CPU SA Voltage` and
`CPU IO Voltage` (resp. `1.050V` and `0.950V`).

MSI, your ClickBIOS *sucks*.

# Noise

Noise is annoying, if it comes from your machine, and continues after you unplug
your case fans. *Yup*, the noise was caused by my NH-D14 cooler. So I used the
[*U.L.N.A*](https://noctua.at/en/nh-d14/specification) (Ultra Low Noise
Adaptater) that came with the cooler to reduce fan rotation speed (and thus,
noise).

Noctua, clearly I expected better!

# Overclocking

The i5 6600K is made for overclocking, and given my 75€ investment in the NH-D14
cooler, I would be a fool not to even try.

![Overclocking decision tree, see [the
source](https://git.sr.ht/~moviuro/try.popho.be/tree/master/images/oc-method.dot)](/images/oc-method.svg)

|Multiplier|Smallest stable V~core~|°C top percentile|
| - | - | - |
| 38 | 1.100V | ?°C |
| 39 | 1.110V | ?°C |
| 40 | 1.140V | ?°C |
| 41 | 1.170V | 62°C |
| 42 | 1.280V^not\ a\ typo^ | ?°C |
| 43 | 1.305V | 75°C |

In the end, I stopped at 4.1GHz (a ~17% increase in frequency).
