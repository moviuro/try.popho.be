---
title: Switching to pandoc
author: Moviuro
keywords:
- pandoc
- projects
date: 2017-03-29
lang: en-US
---

I recently discovered that I had lost the source files and tools to generate the
previous version of my website; so I took the opportunity to try `pandoc` and so
far so good.

I wrote a wrapper script to generate the pages, tore some of my hair out when I
had to use `jq(1)` to extract the titles and generate the navigation entries.

There are surely things to enhance, but I'll be getting there. However right
now, I can be proud of:

* the whole site as of today is less than 100kB
* no more JS
