---
title: PSD2 was meant to enhance security for users
subtitle: how PSD2 will only irritate users
author: Moviuro
keywords:
- MFA
- securitywashing
date: 2020-06-18
lang: en-US
---

# what is PSD2?

See the [Wikipedia
article](https://en.wikipedia.org/wiki/Payment_Services_Directive#Revised_Directive_on_Payment_Services_(PSD2))
about it. In particular, I'll be writing a bit about [*Strong Customer
Authentication*](https://en.wikipedia.org/wiki/Strong_customer_authentication)
(SCA).

Long story short: the [European Banking Authority](https://eba.europa.eu/)
published some strict rules European banks must adhere to; both my banks read
the rules like idiots and applied said rules in a half-assed manner: security
feeling was boosted, security wasn't.

# sanely done

This digraph below shows what serious, good, secure 2FA is. This setup relies on
both a *knowledge* factor (the login + password) and a *possession* factor
(either the machine holding the 2FA cookie or the device that can receive its
challenge -- usually a phone).

![Good 2FA digram, see
[dot
source](https://git.sr.ht/~moviuro/try.popho.be/blob/master/images/2fa.dot)](/images/2fa.svg)

It's not a silver bullet, it's not impervious to [SIM
swap](https://krebsonsecurity.com/tag/sim-swap/). But bear with me: it's so much
better than what follows.

# not sanely done

The digraph below is the insanity that both my French banks used on me just last
week. Where should I begin?

![Insane and unsafe 2FA digraph, see
[dot
source](https://git.sr.ht/~moviuro/try.popho.be/blob/master/images/not2fa.dot)](/images/not2fa.svg)

This is insane, right? If not, consider this series of events:

* My login and PIN leak online (this happens quite often, and French banks use
  6-digit PINs, not passwords)
* I use my own, safe, secure computer at home to open my session
* Because I'm me, and the OTP challenge actually works, I pass that challenge
* I browse my account, get my files, etc.
* I logout
* A fraudster somewhere on Earth (or Mars, whatever) uses my leaked credentials

What happens now?

* Good, sane 2FA ([previous §](#sanely-done)): fraudster must answer a challenge
  that I received, I alert my bank, change my PIN, boom: crisis averted
* Insane, unsafe, not 2FA: no challenge is issued, fraudster has access to my
  account until the 90 days run out

**This was just the entire fucking point.  My banks both plainly missed the
entire point of good SCA.**

*But Moviuro,* you might say, *[PSD2 only
states](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32018R0389#d1e661-23-1):*

> For the purpose of paragraph 1, payment service providers shall not be
> exempted from the application of strong customer authentication where either
> of the following condition is met:
>
> (a) *[snip]*
>
> (b) more than 90 days have elapsed since the last time the payment service
> user accessed online the information specified in paragraph 1(b) and strong
> customer authentication was applied.

Yes, but then, why would my own banks also write the following?

> Dolt Banking enhances the security of your online accounts, in accordance with
> the European Directive on Payment Services.

Or this lie?

> As of June 8, strong authentication will be required to access your accounts.

Occam's razor maybe? I prefer the term *"securitywashing"* (a neologism, see
[Greenwashing](https://en.wikipedia.org/wiki/Greenwashing)).

# my own experience

I'm working at a bank right now that has yet to roll out its PSD2-mandated SCA.
They were just about to make the same mistake.  And it takes a metric ton of
work and sweat to change its trajectory.  I'm not even sure it'll succeed.  But
I don't care anymore.  I've warned whoever would listen, facerolled on my
keyboard in despair more than once, and arrived to this conclusion: legal wants
compliance, and IT stupidly puts in place exactly what legal asks.  The PSD2
team literally told me: *"We don't care one bit about customer security.  PSD2
will only irritate them more."*

Sure, PSD2 is about more than just 2FA.  It's about [~open
banking](https://www.mckinsey.com/industries/financial-services/our-insights/data-sharing-and-open-banking),
too, and more stuff I don't understand.

I'm not naming my banking establishments, but clearly those who could stop that
masquerade have turned a blind eye to it.  **The very teams behind the
authentication mechanisms didn't see the joke that they are!**

Sure, banking establishments have a ton of things to manage: compliance and SCA
are just a tiny tiny part of it.  However, I question the mental sanity of
the people following the PSD2 compliance project: it would have been the best
time to push for enhanced security, not just enhanced security feeling.

I don't care about excuses though.  PSD2 badly implemented will only hurt both
the bank and the customer in the long run:

* customers will have the feeling that their accounts are secure (they aren't,
  but the bank's still liable, so you shouldn't care too much)
* PSD2 was supposed to make fraud more difficult (this will be a surprise for
  the risk department... though, do you have any idea how much money is lost
  because "enterprise" and "professional" banking customer don't have good SCA?)
* compliance did cost a lot of money already (ROI will probably be nil)

Is your own bank PSD2-compliant? Did it already provide a way to do [correct
MFA](https://2fa.directory/#banking)?
