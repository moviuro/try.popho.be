---
title: "New machine: Castle of Hearts"
subtitle: How I got to install FreeBSD on a machine with a broken console
author: Moviuro
keywords:
- machine
- projects
- freebsd
date: 2017-10-16
lang: en-US
---

# presentation

So in late September, I acquired a [HPE microserver
Gen10](https://archive.md/xhPZE) because:

* it's tiny
* it's cheap (267€, incl. taxes & shipping)
* it's got 4 SATA ports

So it's a sweet little black cube of what should have been pure awesomeness.
Slap four
[WD40EZRZ](https://www.wdc.com/products/internal-storage/wd-blue-pc-desktop-hard-drive.html#WD40EZRZ)
in this bad boy and you get a sort of really cheap home NAS system.

*ONWARDS*, let's install FreeBSD!

# install

[Haha, well shit.](https://bugs.freebsd.org/bugzilla/show_bug.cgi?id=221350)
Console is broken, so there's **no** hope in doing a blind install because the
installer requires a console to actually run. The bug report being thorough and
complete, though, I quickly jumped to my only other known fallback system:
[*mfsbsd*](http://mfsbsd.vx.sk/).

*mfsbsd* does two things that the official boot media doesn't:

* it starts `sshd` at boot
* it launches `dhclient` at boot on all interfaces

With a bit of `arp -a`, it was easy to identify the new machine, `ssh` into it,
and run `bsdinstall` from there. Only problem was that `bsdinstall` complained
about a missing file at `/usr/freebsd-dist/MANIFEST` that I had to download from
elsewhere (a FreeBSD mirror).

# (fun) facts

* those were [Torx](https://en.wikipedia.org/wiki/Torx) screws. Screw you HP.
* downloading the latest BIOS update is impossible. Fuck you, HP.
* Ubuntu booted fine with a working console on the machine, but couldn't see the
HDDs. Rather problematic for a NAS system.
* FreeBSD had a broken console but works otherwise fine.
