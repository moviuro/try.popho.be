---
title: Bye Ads
subtitle: How I got to worry less about bad domains
author: Moviuro
keywords:
- ads
- projects
- dns
date: 2016-06-16
lang: en-US
---

For a short while now, I’ve been slowly killing news writers. Don’t get me
wrong, I didn’t shoot anyone: I just made it impossible to reach their ads’
servers from my networks. So they don’t earn money from me.

Call me heartless or whatever, have a look yourself:
[malvertising](https://en.wikipedia.org/wiki/Malvertising) and also this rather
unsettling
["discovery"](https://plus.google.com/+ArtemRussakovskii/posts/7jMWV7oCQpn) (Ads
use bandwidth! a lot!)

I wrote a [tiny^?^
script](https://github.com/moviuro/moviuro.bin/blob/master/lie-to-me) that
generates lying DNS files for `unbound`, `bind` or simply your `hosts(5)` file.
You may clone the repo, as I might or might not update these scripts in the near
future:

    $ git clone https://github.com/moviuro/moviuro.bin

Usage is stupidly simple (I like writing stuff with a simple interface):

    $ lie-to-me [-f unbound|bind|hosts|none] [-o path] [-t IP]

To avoid `404s` or `504s`, you should have a webserver serving `204` on the `IP`
address. (`204` means *No Content*, and is thus the shortest answer that will
not show up as errors in your web browser, see
[here](http://www.shadowandy.net/2014/04/adblocking-nginx-serving-1-pixel-gif-204-content.htm)
for an example setup)

A really great side effect is also that I completely block known bad servers
(malware C&C, “shock sites”, etc.) on all machines on my network (that means my
AndroidTV, my Android phone, my GF’s Windows machine, my router). Free and
painless! What else could you possibly ask for?
