---
title: OpenVPN FAQ
author: Moviuro
keywords:
- vpn
date: 2017-05-29
lang: en-US
---

On [r/OpenVPN](https://reddit.com/r/OpenVPN), I see numerous posts about simple
issues that people encounter. I hope to tackle the most frequently asked ones
here.

# Keys and Certificate Authority

If you're using login/password on your own instance, think again: they can be
brute-forced. They're a pain to remember and really don't have anything more
that certificate-based authentication doesn't.

**Use certificates. [This
guide](https://openvpn.net/index.php/open-source/documentation/miscellaneous/77-rsa-key-management.html)
is great.**

# Reaching your LAN over the VPN

In these scenarii, you have control over the VPN server.

## Everyone is a client

A very simple case would be that you want to reach one of the VPN clients. You
simply need to have the `client-to-client` directive in the *server* config
file. That way you (home-away) can reach the other machine, because you're part
of the same network. Nothing difficult here.

![Client to client communication](/images/vpn-client-to-client.svg)

We can expand on this example and connect to several services across a number of
hosts at different locations.

![Client to client communication,
extended](/images/vpn-client-to-client-many.svg)

## VPN server that you control at home

### My router is the VPN server at home

![Home router is the VPN server](/images/home-router-is-server.svg)

Because your VPN server is the one handling all the traffic at home, it sees
*incoming* communication from your home-away client to the internal resources,
and also the replies of your home devices. It can then direct them correctly to
you.

If you have issues with this setup, it's most probably **firewalling**.

### My VPN server is not the router

In this scenario, you must enable port forwarding: any incoming packet from the
outside world (where your client is) on the OpenVPN port (udp/1194) must be sent
to your VPN server inside.

![Doing some PAT to reach the server](/images/PAT.svg)

Now, what happens when you try to reach your internal network?

First, the client must be told that the LAN is reachable over the LAN. Read
the [man page](https://community.openvpn.net/openvpn/wiki/Openvpn24ManPage). You
should end up with something like `push "route 192.168.0.0 255.255.255.0"`
inside the server configuration

![The VPN server is inside your home LAN](/images/vpn-server-inside.svg)

In this setup, the client outside is connected to your internal VPN server and
tries to access the home NAS. With no further configuration, this *will* fail
because the home NAS has no idea where to send the answer to the VPN client,
even though the client knows how to reach the home NAS.

To achieve an actual connection to inside resources, your home router must be
configured to push the static route to the VPN subnet as part of the DHCP lease.
See [static-routes](http://man.openbsd.org/dhcp-options.5#option_static-routes).
Note that most consummer-grade routers have a shiny web interface for this job
(e.g.
[Netgear](http://documentation.netgear.com/dg834n/enu/202-10197-02/Advanced.7.4.html)).

![This time we got our routes straight](/images/vpn-server-inside-ok.svg)

